Below were taken from INSTALL.md provided by upstream:

## Usage

### Desktop environments

#### In KDE:

 1. Select Kvantum from *System Settings → Application Style → Widget Style* and apply it.
 2. Select Kvantum from *System Settings → Color → Scheme* and click Apply. You could change the color scheme later if you choose another Kvantum theme with *Kvantum Manager* (see "Using Other Themes" below).

Logging out and in would be good for Plasma to see the new theme.

#### In LXQt:

Just select *kvantum* from *Configuration Center → Appearance → Widget Style*. Kvantum Manager is also shown in Configuration Center for changing the Kvantum theme.

In case you use Compton as the X compositor (not recommended), be sure to disable its shadow and blurring for composited Qt menus with lines like these in `~/.config/compton.conf`:

    shadow-exclude = [ "argb && (_NET_WM_WINDOW_TYPE@:a *= 'MENU' || _NET_WM_WINDOW_TYPE@:a *= 'COMBO')" ];
    blur-background-exclude = [ "(_NET_WM_WINDOW_TYPE@:a *= 'MENU' || _NET_WM_WINDOW_TYPE@:a *= 'COMBO')" ];

#### In Other DEs:

*NOTE: It is highly preferable that you install a Qt configuration utility (which provides a Qt plugin) and select Kvantum with it, instead of using the following methods directly.* Otherwise, there will be no guarantee that any style, other than Qt's default style, will work properly with all Qt applications.

That being said, you could try this command to run a Qt application APP with Kvantum:

    APP -style kvantum

To style all native Qt applications with Kvantum, set the environment variable `QT_STYLE_OVERRIDE` to `kvantum`. For example, you could add this line to your `~/.profile` or `~/.bashrc`:

    export QT_STYLE_OVERRIDE=kvantum

Under GNOME and desktop environments that do not source `~/.profile`, you could make a file like `~/.config/environment.d/qt.conf` with the following line in it and reboot:

    QT_STYLE_OVERRIDE=kvantum

Touching `/etc/environment` is *not* recommended.

### Using other themes

To select or install (as user) Kvantum themes, use *Kvantum Manager*, which is already installed and is available in the start menu as a utility app. It explains each step in a straightforward way. With it, you could not only switch between themes easily but also select themes for specific applications.

For the running parts of KDE/LXQt to recognize the new Kvantum theme, the easiest way is logging out and in again.

### Blur effect (KWin)

The blur effect of any compositor can be used with Kvantum when its active theme has translucent backgrounds. However, Kvantum can control KWin's blur effect with translucent menus and tooltips as well as explicitly translucent windows (like those of QTerminal, Konsole or LXQt Panel). Enabling blur options in *Kvantum Manager* has effect only when KWin's blur effect is enabled; otherwise, there will be no blurring or another compositor will control how translucent backgrounds are blurred.

In the case of compositors other than KWin, it is preferable that menus and tooltips have neither shadow nor blurring because those compositors cannot distinguish the shadows that Kvantum gives to menus and tooltips. But, if disabling menu/tooltip shadow and blurring is not possible with them, *Kvantum Manager → Compositing & General Look → Shadowless menus and tooltips* could be checked as a workaround.

Make sure that you never have two compositors running together! KWin is highly recommended, whether with KDE or with LXQt. It supports both X11 and Wayland and has a decent blur effect.

### Notes for theme makers

If the KDE color scheme of the theme is inside its folder, *Kvantum Manager* will install it too. So, theme makers might want to put these files in their theme folder: `$THEME_NAME.svg`, `$THEME_NAME.kvconfig` and `$THEME_NAME.colors`.

The contents of theme folders (if valid) can also be installed manually in the user's home. The possible installation paths are `~/.config/Kvantum/$THEME_NAME/`, `~/.themes/$THEME_NAME/Kvantum/` and `~/.local/share/themes/$THEME_NAME/Kvantum/`, each one of which takes priority over the next one, i.e. if a theme is installed in more than one path, only the instance with the highest priority will be used by Kvantum.

Themes can also be packaged as deb, rpm, xz,... packages and installed as root:

  1. The possible root installation paths are `/usr/share/Kvantum/$THEME_NAME/` and `/usr/share/themes/$THEME_NAME/Kvantum/` (if `PREFIX=/usr`). The first path will take priority over the second one if a theme is installed in both.
  2. The KDE color schemes should go to  `/usr/share/color-schemes/`.
  3. If a theme is installed both as root and as user, the latter installation will take priority.

Please see [Theme-Making](doc/Theme-Making.pdf) for more information on theme installation paths and their priorities.

The default Qt installation adds several root themes, that can be selected by using *Kvantum Manager*. Their corresponding KDE color schemes are also installed.

## GTK

Kvantum does not — and will not — have any relation to GTK. However, it includes some themes similar to or matching GTK themes.
